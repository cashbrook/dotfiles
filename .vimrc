"cashbrook, 11.16.2019
".vimrc config

syntax on 	"turn on syntax highlighting
set number	"turn on line numbering
set showmatch	"highlight matching brace
set ruler	"show cursor position at all times
set showcmd	"show input of incomplete command
set tabstop=8
set softtabstop=4
set shiftwidth=4
set expandtab

"set colorscheme
colorscheme codedark

"vim-plug
call plug#begin('~/.vim/plugged')
Plug 'mattn/emmet-vim'
Plug 'prettier/vim-prettier', { 'do': 'yarn install' }
call plug#end()

"use % to jump to matching html tag
packadd! matchit

"emmet config
"redefine trigger key
let g:user_emmet_leader_key=','

"netrw config
"let g:netrw_liststyle=3
"let g:netrw_banner=0
